local map = {
	['😏'] = 1,
	['👌'] = 2,
	['😎'] = 4,
	['🤨'] = 8,
	['🤘'] = 16,
	['😐'] = 32,
	['🍆'] = 64,
	['🔞'] = 128
}

local function encode(instr)
	local outstr, count = "", 0
	for i = 1, #instr do
		local n = instr:byte(i)
		for emoji in pairs(map) do
			if n & map[emoji] ~= 0 then
				outstr = outstr..emoji
				count = count + 1
			end
		end

		outstr = outstr..'😈'
		count = count + 1
	end

	return outstr, count
end

local function decode(instr)
	local outstr, byte = "", 0
	while #instr > 0 do
		local emoji = instr:sub(1, 4)
		if emoji == '😈' then
			outstr = outstr .. string.pack('B', byte)
			byte = 0
		else
			byte = byte + map[emoji]
		end

		instr = instr:sub(5)
	end

	return outstr
end

return setmetatable({_Version = 0.2, encode = encode, decode = decode},
		{__call = function(x, ...) return encode(...) end})
